# Contributing to the project

## Development

We are using the docker image `klakegg/hugo`. To pull it please use the following command:

```
docker pull klakegg/hugo:ext-alpine
```

This tag contains the hugo shell and can do more then just build the site. It contains a shell in which you can exec
and develop. If you have Hugo locally installed you are very welcome to also use that.

We are basing our site on the Hugo theme Docsy: https://github.com/google/docsy

If you feel overwhelmed by the mass of files you can have a look at different locations:

- The GH-Example Repo: https://github.com/google/docsy-example
- The example website: https://example.docsy.dev/
- The offical docs: https://www.docsy.dev/docs/

## Workflow

After we have finished the very first version of this site we would like to work in a Fork-MR workflow. Thus meaning
that any work carried out happens on personal forks, and we then receive MR on our Gitlab Repo.

If you submit a MR then please make sure that you try to keep the lines in any file shorter then 120 characters as well
as making sure that the site builds.
If - for example a link - is longer then our line length then please don't force the 120 characters, we are no monsters.
